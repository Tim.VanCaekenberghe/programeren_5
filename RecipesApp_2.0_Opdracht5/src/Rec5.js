import React from 'react';
import {StyleSheet,Image, Button, View, ScrollView} from 'react-native';
import {Content, CardItem, Card, Text, Body} from 'native-base';


class Rec5 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                <Content contentContainerStyle={styles.container}>
                    <Card>
                        <CardItem style={styles.card} header>
                            <Text style={styles.title}>Veggiesatés met champignons en kruidenkaas</Text>
                        </CardItem>
                        <CardItem>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 300, height: 250}}
                                    source={require('../assets/vBrochetten.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem style={styles.card} footer>
                            <Text>
                                1 Laat de cottagecheese uitlekken in een zeef. Borstel de champignons schoon en verwijder de steeltjes. Snipper de lente-ui. Hak de bladpeterselie, tijmblaadjes en rozemarijntakjes.
                                2 Roer de lente-ui, de groene kruiden en 1 el olie door de cottagecheese. Laat het overtollig vocht uitdruipen in de zeef. Kruid naar smaak met peper en zout en zet apart in een kommetje.
                                3 Vul elk champignonhoedje met de kruidenkaas. Prik de champignons op een satéprikker en duw ze stevig tegen elkaar aan. Bestrijk de champignons met wat olijfolie.
                                4 Rooster de satés goudbruin op het rooster in de laagste stand. Leg de champignonsatés op borden en besprenkel ze met wat olijfolie. Werk af met takjes rozemarijn en takjes tijm.
                                ....</Text>
                        </CardItem>
                    </Card>
                    <View style={styles.alternativeLayoutButtonContainer}>
                        <Button style={styles.button}
                                title={"Back"}
                                onPress={() => this.props.switchScreen("VegetarianRecipes")}
                        />
                        <View style={styles.buttonSpace}>
                            <Button style={styles.button}
                                    title={"Home"}
                                    onPress={() => this.props.switchScreen("Homepage")}
                            />
                        </View>
                    </View>
                </Content>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    buttonSpace:{
        marginLeft: 25
    },
    alternativeLayoutButtonContainer: {
        margin: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        flex: 1
    },
    card: {
        width: '100%',
        justifyContent: 'center',
        marginTop: 25
    },
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        textDecorationLine: 'underline'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99',
        padding: 10
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default Rec5;