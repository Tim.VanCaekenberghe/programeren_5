import React from 'react';
import {StyleSheet,Image, Button, View, ScrollView} from 'react-native';
import {Content, CardItem, Card, Text, Body} from 'native-base';


class Rec6 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                <Content contentContainerStyle={styles.container}>
                    <Card style={styles.card}>
                        <CardItem header>
                            <Text style={styles.title}>Pastasalade met gegrilde groenten</Text>
                        </CardItem>
                        <CardItem  style={styles.card}>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 300, height: 250}}
                                    source={require('../assets/vPasta.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem  style={styles.card} footer>
                            <Text>1 Kook de pasta volgens de aanwijzingen op de verpakking. Giet af en laat goed uitlekken en afkoelen.
                                2 Laat de gegrilde groenten uitlekken in een zeefje. Meng ze met de pastaschelpen en breng op smaak met peper en zout.
                                3 Verdeel met behulp van 2 koffielepels de ricotta over de salade en werf af met rucola en een scheutje olijfolie (eventueel de olie uit het potje gegrilde groenten). Serveer meteen.
                            </Text>
                        </CardItem>
                    </Card>
                    <View style={styles.alternativeLayoutButtonContainer}>
                        <Button style={styles.button}
                                title={"Back"}
                                onPress={() => this.props.switchScreen("VegetarianRecipes")}
                        />
                        <View style={styles.buttonSpace}>
                            <Button style={styles.button}
                                    title={"Home"}
                                    onPress={() => this.props.switchScreen("Homepage")}
                            />
                        </View>
                    </View>
                </Content>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    buttonSpace:{
        marginLeft: 25
    },
    alternativeLayoutButtonContainer: {
        margin: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        flex: 1
    },
    card: {
        width: '100%',
        marginTop: 25,
        justifyContent: 'center',
    },
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        textDecorationLine: 'underline'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99',
        padding: 10
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default Rec6;