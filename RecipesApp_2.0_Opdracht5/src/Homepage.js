import React from 'react';
import {StyleSheet, ImageBackground, View, Image} from 'react-native';
import {Content, CardItem, Card, Text} from 'native-base';

let background = require('../assets/groenten.png')
let recepten = require('../assets/groenten1.png')
class Homepage extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={background} style={{width: '100%', height: '100%'}}>
                    <Content contentContainerStyle={styles.container}>
                        <Image source={recepten} style={styles.img}/>
                        <Text style={styles.title}>Soorten gerechten</Text>
                        <View style={styles.row}>
                        <Card style={styles.card}>
                            <CardItem style={styles.card} header bordered>
                                <Text style={styles.text} onPress={() => this.props.switchScreen("MeatRecipes")}>Vleesgerechten</Text>
                            </CardItem>
                        </Card>
                        <Card style={styles.card} >
                            <CardItem header bordered
                            style={styles.card}>
                                <Text style={styles.text} onPress={() => this.props.switchScreen("FishRecipes")}>Visgerechten</Text>
                            </CardItem>
                        </Card>
                            <Card style={styles.card}>
                            <CardItem header bordered
                                      style={styles.card}>
                                <Text style={styles.text} onPress={() => this.props.switchScreen("VegetarianRecipes")} >Vegetarische gerechten</Text>
                            </CardItem>
                            </Card>
                        </View>
                    </Content>
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    title: {
        color: 'black',
        backgroundColor: '#e0e0d1',
        borderRadius: 5,
        opacity: 0.8,
        marginBottom: 75,
        fontSize: 38,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    text: {
        color: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
    card: {
        width: 300,
        height: 75,
        opacity: 0.8,
        justifyContent: 'center',
        borderRadius: 20,
        position: 'relative',

    },
    img:{
       width: '100%',
        marginBottom: 50,
        opacity: 0.8,
        aspectRatio: 3,
        resizeMode: 'contain',


    }
});





export default Homepage;

