import React from 'react';
import {StyleSheet,Image, Button, View, ScrollView} from 'react-native';
import {Content, CardItem, Card, Text, Body} from 'native-base';



class Rec1 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                <Content contentContainerStyle={styles.container}>
                    <Card style={styles.card}>
                        <CardItem style={styles.card} header>
                            <Text style={styles.title}>Balletjes in tomatensaus</Text>
                        </CardItem>
                        <CardItem style={styles.card}>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 300, height: 250}}
                                    source={require('../assets/balletjesInTomatenSaus.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem  style={styles.card} footer>
                            <Text>Vleesballetjes maken:
                                1 klont boter
                                ½ kilogram gehakt: half varken, half kalf of rund
                                wat tarwebloem in een bord om de gerolde balletjes te bebloemen
                                Saus ingrediënten:
                                1 ajuin
                                1 knoflookteentje
                                4 verse tomaten
                            ....</Text>
                        </CardItem>
                    </Card>
                    <View style={styles.alternativeLayoutButtonContainer}>
                    <Button style={styles.button}
                        title={"Back"}
                            onPress={() => this.props.switchScreen("MeatRecipes")}
                    />
                    <View style={styles.buttonSpace}>
                    <Button style={styles.button}
                        title={"Home"}
                            onPress={() => this.props.switchScreen("Homepage")}
                    />
                    </View>
                    </View>
                </Content>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    buttonSpace:{
        marginLeft: 25
    },
    alternativeLayoutButtonContainer: {
        margin: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
     flex: 1
    },
    card: {
        marginTop: 25,
    width: '100%',
        justifyContent: 'center',
},
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        textDecorationLine: 'underline'

    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99',
        padding: 10
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default Rec1;