import React from 'react';
import {StyleSheet,Image, Button, View, ScrollView} from 'react-native';
import {Content, CardItem, Card, Text, Body} from 'native-base';

class MeatRecipes extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                <Text style={styles.title}>Vleesgerechten</Text>
                <Content contentContainerStyle={styles.viewcontainer}>
                    <Card style={styles.card} >
                    <CardItem style={styles.card}  header>
                        <Text style={styles.title1}>Balletjes in Tomatensaus</Text>
                    </CardItem>
                    <CardItem style={styles.card} >
                        <Body style={styles.body}>
                            <Image
                                style={{width: 150, height: 150}}
                                source={require('../assets/balletjesInTomatenSaus.png')}
                            />
                        </Body>
                    </CardItem>
                    <CardItem style={styles.card}  footer>
                        <Content>
                            <Button
                                title="Recept"
                                style={styles.button}
                                onPress={() => this.props.switchScreen("Rec1")}/>
                        </Content>
                    </CardItem>
                </Card>
                    <Card>
                        <CardItem header style={styles.card} >
                            <Text style={styles.title1}>Varkenshaasje met aardappelgratin</Text>
                        </CardItem>
                        <CardItem style={styles.card} >
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 150, height: 150}}
                                    source={require('../assets/varkenshaasje.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem style={styles.card}  footer>
                            <Content>
                                <Button
                                    title="Recept"
                                    style={styles.button}
                                    onPress={() => this.props.switchScreen("Rec2")}/>
                            </Content>
                        </CardItem>
                    </Card >
                    <Button title={"Go back"}
                            onPress={() => this.props.switchScreen("Homepage")}
                    />
                </Content>
                </ScrollView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 10,
        marginBottom: 5,
        textDecorationLine: 'underline'
    },
    viewcontainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99',
        margin: 20
    },
    title1: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99'
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    },
    button:{
        width: 100,
        opacity: 0.8,
    },
    card: {
    width: '100%',
        justifyContent: 'center'
}
});

export default MeatRecipes;