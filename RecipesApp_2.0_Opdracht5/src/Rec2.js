import React from 'react';
import {StyleSheet,Image, Button, View, ScrollView} from 'react-native';
import { Content, CardItem, Card, Text, Body} from 'native-base';


class Rec2 extends React.Component {
    render() {
        return (
            <ScrollView>
            <View style={styles.container}>
                <Content contentContainerStyle={styles.container}>
                    <Card style={styles.card}>
                        <CardItem style={styles.card} header>
                            <Text style={styles.title}>Varkenshaasje met aardappelgratin</Text>
                        </CardItem>
                        <CardItem>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 300, height: 250}}
                                    source={require('../assets/varkenshaasje.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem style={styles.card} footer>

                            <Text>
                                Recept
                                Verwarm de melk op een zacht vuur samen met de room, rozemarijn, tijm en 2 teentjes look. Kruid met peper, zout en muskaatnoot. Laat alle kruiden hun smaak afgeven aan de room en melk. Het mengsel mag heet worden, maar niet koken.
                                Schil intussen de aardappelen en snijd in dunne plakjes, eventueel met een mandoline.
                                Verwarm de oven op 160°C.
                                Beboter een ovenschaal en leg de aardappelschijfjes dakpansgewijs in de schotel. Zeef het mengsel van melk en room en schenk het over de aardappelschijfjes tot ze onderstaan.
                                Werk af met de gemalen kaas.
                                Plaats de schaal in de oven en laat 35 minuten garen op 160°.
                                ....</Text>

                        </CardItem>
                    </Card>
                    <View style={styles.alternativeLayoutButtonContainer}>
                        <Button style={styles.button}
                                title={"Back"}
                                onPress={() => this.props.switchScreen("MeatRecipes")}
                        />
                        <View style={styles.buttonSpace}>
                            <Button style={styles.button}
                                    title={"Home"}
                                    onPress={() => this.props.switchScreen("Homepage")}
                            />
                        </View>
                    </View>
                </Content>
            </View>
    </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    buttonSpace:{
        marginLeft: 25
    },
    alternativeLayoutButtonContainer: {
        margin: 30,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        flex: 1
    },
    card: {
        width: '100%',
        justifyContent: 'center',
        marginTop: 25
    },
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        justifyContent: 'center',
        textDecorationLine: 'underline'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99',
        padding: 10
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default Rec2;