import React from 'react';
import {StyleSheet,Image, Button, View} from 'react-native';
import { Content, CardItem, Card, Text, Body} from 'native-base';


class Rec2 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Content contentContainerStyle={styles.container}>
                    <Card>
                        <CardItem header>
                            <Text style={styles.title}>Varkenshaasje met aardappelgratin</Text>
                        </CardItem>
                        <CardItem>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 150, height: 150}}
                                    source={require('../assets/varkenshaasje.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>
                                Recept
                                Verwarm de melk op een zacht vuur samen met de room, rozemarijn, tijm en 2 teentjes look. Kruid met peper, zout en muskaatnoot. Laat alle kruiden hun smaak afgeven aan de room en melk. Het mengsel mag heet worden, maar niet koken.
                                Schil intussen de aardappelen en snijd in dunne plakjes, eventueel met een mandoline.
                                Verwarm de oven op 160°C.
                                Beboter een ovenschaal en leg de aardappelschijfjes dakpansgewijs in de schotel. Zeef het mengsel van melk en room en schenk het over de aardappelschijfjes tot ze onderstaan.
                                Werk af met de gemalen kaas.
                                Plaats de schaal in de oven en laat 35 minuten garen op 160°.
                                ....</Text>
                        </CardItem>
                    </Card>
                    <Button title={"Go back"}
                            onPress={() => this.props.switchScreen("MeatRecipes")}
                    />
                </Content>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99'
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default Rec2;