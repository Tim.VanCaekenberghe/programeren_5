import React from 'react';
import {StyleSheet,Image, Button, View} from 'react-native';
import {Content, CardItem, Card, Text, Body} from 'native-base';


class Rec1 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Content contentContainerStyle={styles.container}>
                    <Card>
                        <CardItem header>
                            <Text style={styles.title}>Gebakken tongreepjes met rucolasalade</Text>
                        </CardItem>
                        <CardItem>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 150, height: 150}}
                                    source={require('../assets/tongrolletjes.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>
                                Bereiding
                                Spoel de tongfilets af onder koud water en dep ze goed droog. Snijd de filets in lange repen. Bestrooi ze met zout, een beetje peper en bloem.
                                Maak de sla schoon, scheur de grote bladeren iets kleiner en meng deze met de rucola. Pel de rode ui, verdeel deze in ringen en leg ze op de sla.
                                Rooster de pijnboompitten in een droge koekenpan goudbruin.
                                Meng de ingrediënten voor de dressing en schep deze door de salade.
                                Bak de tongreepjes 3-4 minuten in de hete olie. Schep ze uit de pan en verdeel ze als ze lauwwarm zijn over de salade. Strooi de pijnboompitten erover.
                                Lekker met ciabatta of stokbrood.
                                </Text>
                        </CardItem>
                    </Card>
                    <Button title={"Go back"}
                            onPress={() => this.props.switchScreen("FishRecipes")}
                    />
                </Content>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99'
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default Rec1;