import React from 'react';
import {StyleSheet,Image, Button, View} from 'react-native';
import {Content, CardItem, Card, Text, Body} from 'native-base';


class Rec6 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Content contentContainerStyle={styles.container}>
                    <Card>
                        <CardItem header>
                            <Text style={styles.title}>Pastasalade met gegrilde groenten</Text>
                        </CardItem>
                        <CardItem>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 150, height: 150}}
                                    source={require('../assets/vPasta.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>1 Kook de pasta volgens de aanwijzingen op de verpakking. Giet af en laat goed uitlekken en afkoelen.
                                2 Laat de gegrilde groenten uitlekken in een zeefje. Meng ze met de pastaschelpen en breng op smaak met peper en zout.
                                3 Verdeel met behulp van 2 koffielepels de ricotta over de salade en werf af met rucola en een scheutje olijfolie (eventueel de olie uit het potje gegrilde groenten). Serveer meteen.
                            </Text>
                        </CardItem>
                    </Card>
                    <Button title={"Go back"}
                            onPress={() => this.props.switchScreen("VegetarianRecipes")}
                    />
                </Content>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99'
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default Rec6;