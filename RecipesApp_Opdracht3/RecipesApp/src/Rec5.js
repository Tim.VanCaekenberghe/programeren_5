import React from 'react';
import {StyleSheet,Image, Button, View} from 'react-native';
import {Content, CardItem, Card, Text, Body} from 'native-base';


class Rec5 extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Content contentContainerStyle={styles.container}>
                    <Card>
                        <CardItem header>
                            <Text style={styles.title}>Veggiesatés met champignons en kruidenkaas</Text>
                        </CardItem>
                        <CardItem>
                            <Body style={styles.body}>
                                <Image
                                    style={{width: 150, height: 150}}
                                    source={require('../assets/vBrochetten.png')}
                                />
                            </Body>
                        </CardItem>
                        <CardItem footer>
                            <Text>
                                1 Laat de cottagecheese uitlekken in een zeef. Borstel de champignons schoon en verwijder de steeltjes. Snipper de lente-ui. Hak de bladpeterselie, tijmblaadjes en rozemarijntakjes.
                                2 Roer de lente-ui, de groene kruiden en 1 el olie door de cottagecheese. Laat het overtollig vocht uitdruipen in de zeef. Kruid naar smaak met peper en zout en zet apart in een kommetje.
                                3 Vul elk champignonhoedje met de kruidenkaas. Prik de champignons op een satéprikker en duw ze stevig tegen elkaar aan. Bestrijk de champignons met wat olijfolie.
                                4 Rooster de satés goudbruin op het rooster in de laagste stand. Leg de champignonsatés op borden en besprenkel ze met wat olijfolie. Werk af met takjes rozemarijn en takjes tijm.
                                ....</Text>
                        </CardItem>
                    </Card>
                    <Button title={"Go back"}
                            onPress={() => this.props.switchScreen("VegetarianRecipes")}
                    />
                </Content>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffdd99'
    },
    text: {
        color: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        alignItems: 'center'
    }
});

export default Rec5;