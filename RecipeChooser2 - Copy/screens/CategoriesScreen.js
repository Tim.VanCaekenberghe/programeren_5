import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Button, FlatList } from 'react-native';
import { CommonActions } from '@react-navigation/native';

import DefaultStyle from "../constants/default-style";
import Colors from '../constants/colors';
import CategoryMealScreen from "./CategoryMealScreen";

import {CATEGORIES} from "../data/dummy-data";

const renderGridItem = (itemData) =>{
    return <View style={styles.gridItem}><Text>{itemData.item.title}</Text></View>;
}
const categoryGridTile = props => {
    return (
        <TouchableOpacity
        style={styles.gridItem}>
            <View style={{backgroundColor: props.color}}>
                <Text>{props.title}</Text>
            </View>
        </TouchableOpacity>
    )
}

const CategoriesScreen = props =>{
    return(
        <FlatList data={CATEGORIES} renderItem={renderGridItem} numColumns={2}/>
    );

};

const styles = StyleSheet.create({
gridItem:{
    flexDirection: 'row',
    flex: 1,
    padding: 10,
    marginBottom: 70

}
});

export default CategoriesScreen;
