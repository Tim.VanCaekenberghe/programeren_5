import React from 'react';
import {StyleSheet, Text, View, ImageBackground, ScrollView, Image} from 'react-native';
import {Content, Accordion, ListItem, List} from 'native-base'
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import StarRating from 'react-native-star-rating'

const Drawer = createDrawerNavigator()

function HomeScreen({navigation}) {
    let myBackground = require('./assets/fietbg.png');
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ImageBackground source={myBackground} style={{opacity: 0.6, width: '100%', height: '100%'}}>
                <Content
                    contentContainerStyle={{marginTop: 150, textAlign: 'center', alignItems: 'center', opacity: 1}}>
                    <Text style={{color: 'black', margin: 15, borderWidth: 1, opacity: 0.9,  backgroundColor: 'lightgrey',marginTop: 10, fontSize: 24, fontWeight: 'bold'}}>Fietsroutes
                        - Vlaamse Ardennen</Text>
                    <Image
                        source={{uri: 'https://cdn.dribbble.com/users/10896/screenshots/2613703/cyclist-on-bike-slower.gif'}}
                        style={{width: 200, height: 200, opacity: 1}}
                    />
                    <Content
                        contentContainerStyle={{borderWidth: 1, alignItems:'center', marginTop: 20, opacity:0.9, backgroundColor: 'lightgrey'}}>
                    <Text style={{color: 'black', marginTop: 5, fontSize: 22, fontWeight: 'bold'}}>Onze fietsroutes ontdekken?</Text>
                    <Text style={{color: 'blue', marginTop: 5, fontSize: 18, fontWeight: 'bold'}}>Swipe van links naar
                        rechts</Text>
                    </Content>
                </Content>
            </ImageBackground>
        </View>
    )
}

function Route1() {
    let backgroundR1 = require('./assets/eddy-merckx1.png');
    const dataArray = [
        {title: "Afstand", content: "46,0 km"},
        {title: "Reliëf", content: "Steil"},
        {title: "Type", content: "Uitdagend"},
        {title: "Startplaats", content: "Ruienplein 9690 Kluisbergen"}
    ];
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ScrollView>
            <Content>
                <Text style={{
                    color: 'black',
                    textAlign: 'center',
                    margin: 45,
                    marginTop: 40,
                    marginBottom: 20,
                    fontSize: 22
                }}>EDDY MERCKX FIETSROUTE</Text>
                <Image source={backgroundR1} style={{width: '100%', height: 240}}/>
            </Content>
        <Content contentContainerStyle={{width: '100%', marginTop: 5, alignItems: 'center'}}>
            <Text style={{textAlign: 'center', fontSize: 15, marginBottom: 5}}>Moeilijkheid:</Text>
        < StarRating
    maxStars = {5}
    rating = {4}
    starSize = {15}
    fullStarColor={'gold'}
    starStyle={{marginBottom: 10}}
    />
</Content>
                <Content>
                    <List>
                        <ListItem itemDivider>
                            <Text style={{fontWeight: 'bold'}}>Beschrijving van deze rit:</Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} Proef van de Ronde van Vlaanderen op deze sportieve tocht door de Vlaamse Ardennen.
                            </Text>
                        </ListItem>
                        <ListItem>
                        <Text>
                            {'\u2022'} In startpunt Ruien won wielerheld Eddy Merckx zijn laatste wedstrijd.
                        </Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} Beklim 4 bekende hellingen uit Vlaanderens Mooiste, de Oude Kwaremont, de Paterberg, de Hotondberg en de Kluisberg.
                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} Een route langs prachtige natuur en vergezichten.
                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>Informatie van deze rit:</Text>
                        </ListItem>
                    </List>
                </Content>
                <Content contentContainerStyle={{width: '100%'}}>
                    <Accordion dataArray={dataArray} icon="add"  expandedIcon="remove"/>
                </Content>
            </ScrollView>
        </View>
)
}
function Route2() {
    let backgroundR2 = require('./assets/Oude-kwaremont.png');
    const dataArray = [
        {title: "Afstand", content: "78,0 km"},
        {title: "Reliëf", content: "Steil"},
        {title: "Type", content: "Uitdagend"},
        {title: "Startplaats", content: "Markt 43 9700 Oudenaarde"}
    ];
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ScrollView>
                <Content>
                    <Text style={{
                        color: 'black',
                        textAlign: 'center',
                        margin: 45,
                        marginTop: 40,
                        marginBottom: 20,
                        fontSize: 22
                    }}>RONDE VAN VLAANDEREN FIETSROUTE: BLAUWE LUS</Text>
                    <Image source={backgroundR2} style={{width: '100%', height: 240}}/>
                </Content>
                <Content contentContainerStyle={{width: '100%', marginTop: 5, alignItems: 'center'}}>
                    <Text style={{textAlign: 'center', fontSize: 15, marginBottom: 5}}>Moeilijkheid:</Text>
                    < StarRating
                        maxStars = {5}
                        rating = {5}
                        starSize = {15}
                        fullStarColor={'gold'}
                        starStyle={{marginBottom: 10}}
                    />
                </Content>
                <Content>
                    <List>
                        <ListItem itemDivider>
                            <Text style={{fontWeight: 'bold'}}>Beschrijving van deze rit:</Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} Beleef je eigen Ronde in het decor van Vlaanderens Mooiste.
                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} 3 lussen verbinden de belangrijkste hellingen en kasseistroken uit de Rondefinale.

                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} Volg de speciale borden en proef van het betere klimwerk.
                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} De blauwe lus bevat stevige hellingen als de Oude Kwaremont, de Koppenberg en de Taaienberg.
                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>Informatie van deze rit:</Text>
                        </ListItem>
                    </List>
                </Content>
                <Content contentContainerStyle={{width: '100%'}}>
                    <Accordion dataArray={dataArray} icon="add"  expandedIcon="remove"/>
                </Content>
            </ScrollView>
        </View>
    )
}
function Route4() {
    let backgroundR3 = require('./assets/bidonroute-muur.png');
    const dataArray = [
        {title: "Afstand", content: "42,0"},
        {title: "Reliëf", content: "Glooiend"},
        {title: "Type", content: "Sportief"},
        {title: "Startplaats", content: "Brugstraat 1 9500 Geraardsbergen"}
    ];
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ScrollView>
                <Content>
                    <Text style={{
                        color: 'black',
                        textAlign: 'center',
                        margin: 45,
                        marginTop: 40,
                        marginBottom: 20,
                        fontSize: 22
                    }}>DE MOOISTE RONDES - BIDON FIETSROUTE</Text>
                    <Image source={backgroundR3} style={{width: '100%', height: 240}}/>
                </Content>
                <Content contentContainerStyle={{width: '100%', marginTop: 5, alignItems: 'center'}}>
                    <Text style={{textAlign: 'center', fontSize: 15, marginBottom: 5}}>Moeilijkheid:</Text>
                    < StarRating
                        maxStars = {5}
                        rating = {3}
                        starSize = {15}
                        fullStarColor={'gold'}
                        starStyle={{marginBottom: 10}}
                    />
                </Content>
                <Content>
                    <List>
                        <ListItem itemDivider>
                            <Text style={{fontWeight: 'bold'}}>Beschrijving van deze rit:</Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} De Bidonroute passeert voldoende wielercafés om onderweg de dorst te laven.
                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} Om het veilig te houden, is de route niet langer dan 42 kilometer.
                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} Starten doe je in Geraardsbergen, de thuishaven van de mattentaart.
                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>Informatie van deze rit:</Text>
                        </ListItem>
                    </List>
                </Content>
                <Content contentContainerStyle={{width: '100%'}}>
                    <Accordion dataArray={dataArray} icon="add"  expandedIcon="remove"/>
                </Content>
            </ScrollView>
        </View>
    )
}
function Route3() {
    let backgroundR3 = require('./assets/eddy-merckx1.png');
    const dataArray = [
        {title: "Afstand", content: "24,0 km"},
        {title: "Reliëf", content: "Vlak"},
        {title: "Type", content: "Rustig"},
        {title: "Startplaats", content: "Galerijpad 2 9630 Zwalm"}
    ];
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ScrollView>
                <Content>
                    <Text style={{
                        color: 'black',
                        textAlign: 'center',
                        margin: 45,
                        marginTop: 40,
                        marginBottom: 20,
                        fontSize: 22
                    }}>WINTERFIETSEN IN DE ZWALMSTREEK</Text>
                    <Image source={backgroundR3} style={{width: '100%', height: 240}}/>
                </Content>
                <Content contentContainerStyle={{width: '100%', marginTop: 5, alignItems: 'center'}}>
                    <Text style={{textAlign: 'center', fontSize: 15, marginBottom: 5}}>Moeilijkheid:</Text>
                    < StarRating
                        maxStars = {5}
                        rating = {1}
                        starSize = {15}
                        fullStarColor={'gold'}
                        starStyle={{marginBottom: 10}}
                    />
                </Content>
                <Content>
                    <List>
                        <ListItem itemDivider>
                            <Text style={{fontWeight: 'bold'}}>Beschrijving van deze rit:</Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} De vlakste van de 4 fietsroutes, doorheen een idyllisch stukje Vlaamse Ardennen, nabij brouwerij Contreras, van het heerlijke bier Valeir.
                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} Makkelijk te bereiken vanuit Gent, via het jaagpad langs de Schelde. Ideaal voor e-bikes!

                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'}Kleine charmante dorpjes, weidse panorama's, de kabbelende zwalmbeek en een prachtige Scheldearm.
                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>
                                {'\u2022'} 2 café-restaurants en 1 prachtig veldcafé. Overal lekkere bieren!
                            </Text>
                        </ListItem>
                        <ListItem>
                            <Text>Informatie van deze rit:</Text>
                        </ListItem>
                    </List>
                </Content>
                <Content contentContainerStyle={{width: '100%'}}>
                    <Accordion dataArray={dataArray} icon="add"  expandedIcon="remove"/>
                </Content>
            </ScrollView>
        </View>
    )
}

export default function App() {
return (
<NavigationContainer>
<Drawer.Navigator drawerType='slide' initialRouteName="HomeScreen"
screenOptions={{
headerStyle: {
backgroundColor: 'red',
},
headerTintColor: 'red',
headerTitleStyle: {
fontWeight: 'bold',
},
}}
>
<Drawer.Screen
name="HomeScreen"
component={HomeScreen}
options={{
title: 'Fietsroutes'

}}
/>
<Drawer.Screen
name="Route1"
component={Route1}
options={{ title: 'Route 1 - Eddy Merckx'}}
/>
    <Drawer.Screen
        name="Route2"
        component={Route2}
        options={{ title: 'Route 2 - Blauwe Lus'}}
    />
    <Drawer.Screen
        name="Route3"
        component={Route3}
        options={{ title: 'Route 3 - Zwalmstreek'}}
    />
    <Drawer.Screen
        name="Route4"
        component={Route4}
        options={{ title: 'Route 4 - Bidon'}}
    />
</Drawer.Navigator>
</NavigationContainer>
);
}

const styles = StyleSheet.create({
container: {
flex: 1,
backgroundColor: '#fff',
alignItems: 'center',
justifyContent: 'center',
},
});
