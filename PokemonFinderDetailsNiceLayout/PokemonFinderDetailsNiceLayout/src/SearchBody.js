import React from 'react';
import {Text, View, ImageBackground, ScrollView, Platform,  Image} from 'react-native';


import {Header, Item, Icon, Input, Button, ListItem, List} from 'native-base';

var searching = require('../assets/search.jpg');


class SearchBody extends React.Component {
    render() {
        return (
            <View>
            <ImageBackground
                source={{uri:"http://pokemongolive.com/img/posts/raids_loading.png"}}
                style={{width: '100%', height: '100%'}}>
            <ScrollView style={{flex:1}}>

                <Text style={styles.header}>NR X - Pokemon Name</Text>
                <View style={styles.viewStyle}>
                    <Image
                        source={searching}
                        style={styles.img}/>

                </View>
                <View style={styles.info}>
                    <ListItem itemDivider>
                        <Text style={{fontWeight: 'bold'}}>Size: </Text>

                    </ListItem>
                    <ListItem>
                        <Text>Weight: x kg </Text>

                    </ListItem>
                    <ListItem>
                        <Text>Height: x m</Text>

                    </ListItem>
                    <ListItem itemDivider>
                        <Text style={{fontWeight: 'bold'}}>Abilities: </Text>
                    </ListItem>

                    <List>
                        <ListItem>
                            <Text>ability 1</Text>
                        </ListItem>
                        <ListItem>
                            <Text>ability 2</Text>
                        </ListItem>
                    </List>

                </View>
            </ScrollView>
            </ImageBackground>
            </View>

        )
    }
}

const styles ={
    header: {
        fontSize: 30,
        color: 'red',
        textAlign:'center'
    },
    viewStyle:{
        justifyContent:'center',
        alignItems:'center',
        flex:1
    },
    img: {
        height: 200,
        width: 200,
        justifyContent:'center',
        alignItems: 'center'
    },
    info: {
        flex: 1,
        backgroundColor:'white',
        opacity:0.6
    }
}

export default SearchBody;
