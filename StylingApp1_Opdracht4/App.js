import React, {Component} from 'react';
import { ImageBackground, StyleSheet, Image, Alert, Button} from 'react-native';
import {Container, Header, Content, Card, CardItem, Text, Body, DatePicker} from 'native-base'

var background = require('./assets/background.png')
var randomImage = require('./assets/Random-Images-Thumb.png')
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { chosenDate: new Date() };
    this.setDate = this.setDate.bind(this);
  }
  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }
  render() {
    return(
      <Container>
        <ImageBackground source={background} style={{width: '100%', height: '100%'}}>
        <Header />
        <Content>
          <Card>
            <CardItem header>
              <Text>Card</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  With
                </Text>
              </Body>
            </CardItem>
            <CardItem footer>
              <Text>Header & footer</Text>
            </CardItem>
          </Card>
          <Card>
            <CardItem header>
              <Text>Card2</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  With
                </Text>
              </Body>
            </CardItem>
            <CardItem footer>
              <Image
              style={{width: 150, height: 150}}
              source={require('./assets/Random-Images-Thumb.png')}
              />
            </CardItem>
          </Card>
          <Button
              title="Doe iets"
              type="outline"
              onPress={() => Alert.alert('Doe niets')}
          />
          <Text>Welke datum is het vandaag?</Text>
          <DatePicker
              defaultDate={new Date(2018, 4, 4)}
              minimumDate={new Date(2018, 1, 1)}
              maximumDate={new Date(2018, 12, 31)}
              locale={"en"}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={"fade"}
              androidMode={"default"}
              placeHolderText="Select date"
              textStyle={{ color: "green" }}
              placeHolderTextStyle={{ color: "#d3d3d3" }}
              onDateChange={this.setDate}
              disabled={false}
          />
          <Text>
            Date: {this.state.chosenDate.toString().substr(4, 12)}
          </Text>
        </Content>
        </ImageBackground>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default App;

