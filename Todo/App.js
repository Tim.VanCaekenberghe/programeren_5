import React from 'react';
import {StyleSheet, Text, View, ImageBackground, TextInput, Button} from 'react-native';

var myBackground = require('./assets/background.png')

class App extends React.Component {
    state = {
        text: "You dont have any todo's yet",
        todo: ["handen wassen", "papieren zakdoek", "social distancing"]
    }
    deleteToDo = (t) => {
        var array = this.state.todo;
        var toDoToDelete = array.indexOf(t);
        array.splice(toDoToDelete, 1);
        this.setState({todo:array})
    }
    addToDo = () => {
        var newTodo = this.state.text;
        var toDoArray = this.state.todo;
        toDoArray.push(newTodo);
        this.setState({todo: toDoArray, text:""})
    }
    createToDoList = () => {
        return this.state.todo.map(
            t => {
                return (
                    <Text key={t}
                          onPress={() => {this.deleteToDo(t)}}
                          style={styles.paragraph}
                          >{t}</Text>
                )
            }
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={myBackground} style={{width: '100%', height: '100%'}}>
                    <Text style={styles.paragraph}>To Do List</Text>
                    <TextInput style={styles.inputStyle}
                               onChangeText={(text) => this.setState({text})}
                               value={this.state.text}
                    />
                    <Button
                        title="Add To Do"
                        onPress={this.addToDo}
                    />
                    {this.createToDoList()}
                </ImageBackground>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    paragraph: {
        color: '#ffffff',
        textAlign: 'center'
    },
    inputStyle: {
        height: 40,
        borderColor: "white",
        borderWidth: 2,
        color: "white"
    }
})


export default App;
