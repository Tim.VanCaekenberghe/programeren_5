import React from 'react';
import {Text, View, ImageBackground, ScrollView, Platform,  Image} from 'react-native';


import {Header, Item, Icon, Input, Button, ListItem, List} from 'native-base';

var searching = require('../assets/search.jpg');


class SearchBody extends React.Component {
    render() {
        let pokemon = this.props.data
        return (
            <View>
            <ImageBackground
                source={{uri:"http://pokemongolive.com/img/posts/raids_loading.png"}}
                style={{width: '100%', height: '100%'}}>
            <ScrollView style={{flex:1}}>

                <Text style={styles.header}>NR {pokemon.id} {pokemon.name.toUpperCase()}</Text>
                <View style={styles.viewStyle}>
                    <Image
                        source={{uri:pokemon.sprites.front_default}}
                        style={styles.img}/>

                </View>
                <View style={styles.info}>
                    <ListItem itemDivider>
                        <Text style={{fontWeight: 'bold'}}>Size: {pokemon.size}</Text>

                    </ListItem>
                    <ListItem>
                        <Text>Weight: {pokemon.weight/10} kg </Text>

                    </ListItem>
                    <ListItem>
                        <Text>Height: {pokemon.height} m</Text>

                    </ListItem>
                    <ListItem itemDivider>
                        <Text style={{fontWeight: 'bold'}}>Abilities: </Text>
                    </ListItem>

                    <List dataArray={pokemon.abilities}
                          renderRow={(item) =>
                              <ListItem>
                              <Text>{item.ability.name}</Text>
                              </ListItem>
                          }>

                    </List>

                </View>
            </ScrollView>
            </ImageBackground>
            </View>

        )
    }
}

const styles ={
    header: {
        fontSize: 30,
        color: 'red',
        textAlign:'center'
    },
    viewStyle:{
        justifyContent:'center',
        alignItems:'center',
        flex:1
    },
    img: {
        height: 200,
        width: 200,
        justifyContent:'center',
        alignItems: 'center'
    },
    info: {
        flex: 1,
        backgroundColor:'white',
        opacity:0.6
    }
}

export default SearchBody;
