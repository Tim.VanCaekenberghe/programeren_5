import React, {useState} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import * as Font from 'expo-font';
import {AppLoading} from 'expo';

import Colors from './constants/colors';

import MealsNavigator from "./navigation/MealsNavigator";



const fetchFonts = () => {
  return Font.loadAsync({
    'bee-leave': require('./assets/fonts/bee-leave.ttf'),
    'highline': require('./assets/fonts/highline.otf'),
    'yummy-ice-cream': require('./assets/fonts/yummy-ice-cream.ttf'),
  });
};


export default function App() {

  const [fontLoaded, setFontLoaded] = useState(false);
  if (!fontLoaded) {
     return (
        <AppLoading
            startAsync={fetchFonts}
            onFinish={() => setFontLoaded(true)}
            onError={(err) => console.log(err)}
        />
    );
  }
  return (
      <NavigationContainer>
          <MealsNavigator/>
      </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:Colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
  }
});
