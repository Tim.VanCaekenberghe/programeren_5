import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';

import DefaultStyle from "../constants/default-style";
import Colors from '../constants/colors';
import meal from '../models/meal'

const CategoryMealScreen = props =>{
const renderMealItem = itemData => {
    return(
        <View>
            <Text>
                {itemData.item.mealName}
            </Text>
        </View>
    )
}
    return(
        <View  styles={DefaultStyle.screen}>
            <Text>
                {props.route.params.mealName} {props.route.params.id}
            </Text>
            <FlatList
            numColumns={1}
            data={meal}
            renderItem={renderMealItem}
            ></FlatList>
        </View>

    );

};

const styles = StyleSheet.create({

});

export default CategoryMealScreen;
