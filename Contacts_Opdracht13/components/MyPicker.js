import React, { useState } from "react";
import {View, Picker, StyleSheet, Text} from "react-native";
import Colors from "../constants/Colors";


const MyPicker = props =>{
    const [selectedValue, setSelectedValue] = useState("");
    return (
        <View style={styles.container}>
            <Text  {...props} style={{ ...styles.text, ...props.style}}>{props.text}</Text>
            <Picker
                style={{ height: 35, width: 200, marginTop: 10, color: Colors.secondary}}
                selectedValue={selectedValue}
                onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
            >
                <Picker.Item label="--Click To Choose--" value="--Click to choose--" />
                <Picker.Item label="A+" value="A+" />
                <Picker.Item label="O+" value="O+" />
                <Picker.Item label="B+" value="B+" />
                <Picker.Item label="AB+" value="AB+" />
                <Picker.Item label="A-" value="A-" />
                <Picker.Item label="O-" value="O-" />
                <Picker.Item label="B-" value="B-" />
                <Picker.Item label="AB-" value="AB-" />
            </Picker>
        </View>
    );
}

const styles = StyleSheet.create({
    text:{
        marginTop: 15,
        color: Colors.secondary,
        fontSize: 15,
        marginRight: 10,
        fontFamily: 'spartan'
    },
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 15,

    }
})
export default MyPicker