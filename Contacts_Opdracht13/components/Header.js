import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Colors from "../constants/Colors";
import MyTitle from "./MyTitle";
const Header = props =>{
  return(
      <View style={styles.header}>
          <MyTitle style={styles.headerTitle}>
              {props.text}
          </MyTitle>
      </View>
  )
}
const styles = StyleSheet.create({
    header:{
        width: '100%',
        backgroundColor: Colors.header,
        alignItems: 'center',
        justifyContent:'center',
        paddingTop: 30,
    },
    headerTitle:{
        fontSize: 35,
        color: Colors.secondary,
        fontWeight: 'bold'
    }
})
export default Header;