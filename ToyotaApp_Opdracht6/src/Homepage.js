import React from 'react';
import {Text, View, ImageBackground, Image, Platform} from 'react-native';
import {Button, Content} from 'native-base';

let background = require('../assets/background.png');


export default class Homepage extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={background} style={{width: '100%', height: '100%', opacity: 0.9}}>
                    <Content contentContainerStyle={styles.viewcontainer}>
                        <Text style={styles.titleStyle}>Welkom! Bekijk hier de modellen</Text>
                        <Text style={styles.text}>Klik hieronder om onze voertuigen te bekijken</Text>
                        <Button
                            block={true}
                            style={styles.buttonStyle}
                            onPress={() => this.props.switchScreen("Cars")}
                        >
                            <Text style={styles.buttonText}>Voertuigen</Text>
                        </Button>
                    </Content>
                </ImageBackground>
            </View>

        )
    }
}

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    titleStyle: {
        fontSize: 25,
        color: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonStyle: {
        margin: 10
    },
    buttonText: {
        color: 'white',
        fontSize: 20
    },
    text: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewcontainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 20
    }
}