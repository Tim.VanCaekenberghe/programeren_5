import React from 'react';
import { View } from 'react-native';
import Homepage from "./src/Homepage";
import Cars from './src/Cars'

export default class App extends React.Component {

    state = {
        currentScreen: "Homepage",
    }
    switchScreen = (currentScreen) => {
        this.setState({currentScreen});
    }


    renderScreen = () => {
        if (this.state.currentScreen === "Homepage") {
            return (
                <Homepage
                    switchScreen={this.switchScreen}
                />
            )
        } else if (this.state.currentScreen === "Cars") {
            return (
                <Cars/>
            )
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderScreen()}
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1
    }
}

