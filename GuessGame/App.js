import React, {Component, useState} from 'react';
import {StyleSheet, View, TouchableWithoutFeedback, Keyboard} from 'react-native';
import StartGameScreen from "./screens/StartGameScreen";
import Header from "./components/Header";
import Home from "./screens/Home";
import Colors from "./constants/Colors";
import GuessingScreen from "./screens/GuessingScreen";
import GameOverScreen from "./screens/GameOverScreen";
import * as Font from 'expo-font';
import {AppLoading} from 'expo';

const fetchFonts = () => {
    return Font.loadAsync({
        'nunito-black': require('./assets/fonts/nunito-black.ttf'),
        'spartan': require('./assets/fonts/spartan.ttf')
    });
};


export default function App() {
   const [dataLoaded, setDataLoaded] = useState(false);
    const[userNumber, setUserNumber] = useState();
    const[guessRounds, setGuessRounds] = useState(0);
    if(!dataLoaded){
        return(
            <AppLoading
                startAsync={fetchFonts}
                onFinish={ ()=> setDataLoaded(true)}/>
        );
    }
    const startGameHandler = (selectedNumber) => {
        setUserNumber(selectedNumber)
    };
    let content = <StartGameScreen onStartGame={startGameHandler}/>
    if (userNumber){
        content= <GuessingScreen userChoice={userNumber}/>
    }


    const GameOverHandler = numberOfRounds =>{
        setGuessRounds(numberOfRounds)
    };
    if (userNumber && guessRounds <= 0) {
        content = <GuessingScreen userChoice={userNumber}
                              onGameOver={GameOverHandler}
        />;
    } else if(guessRounds>0){
        content = <GameOverScreen
        userNumber={userNumber}
        roundNumber={guessRounds}
        /> }

    return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.container}>
                    <Header
                    text={'Guess Number'}
                    />
                    {content}
                </View>
            </TouchableWithoutFeedback>
        );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.secondary,
        fontFamily: 'spartan'
    }
});
