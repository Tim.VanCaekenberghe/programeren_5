import {StyleSheet} from 'react-native';
import Colors from "../constants/Colors";

export default StyleSheet.create({
    bodyText:{
        fontFamily: 'nunito-black',
        color: Colors.accent
    }
})
