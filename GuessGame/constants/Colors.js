export default {
    primary: "#FFC0CB",
    secondary: "#B0E0E6",
    accent: "white",
    normalText: "black"
};