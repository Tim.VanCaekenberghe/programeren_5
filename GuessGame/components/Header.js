import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Colors from "../constants/Colors";
import MyTitle from "./MyTitle";
const Header = props =>{
  return(
      <View style={styles.header}>
          <MyTitle style={styles.headerTitle}>
              {props.text}
          </MyTitle>
      </View>
  )
}
const styles = StyleSheet.create({
    header:{
        width: '100%',
        height: 90,
        backgroundColor: Colors.primary,
        alignItems: 'center',
        justifyContent:'center',
        paddingTop: 30,
    },
    headerTitle:{
        fontSize: 30,
        color: Colors.normalText,
        fontFamily: 'spartan'

    }
})
export default Header;