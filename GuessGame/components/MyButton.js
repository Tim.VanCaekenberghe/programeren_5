import React from 'react'
import {StyleSheet, View, Text, Button} from 'react-native'
import Colors from "../constants/Colors";

const MyButton = props => {
    return (
        <View style={{...styles.button, ...props.style}}>
            <Button
                {...props}
                color={Colors.secondary}
                title={props.text}>
            </Button>
        </View>
    )
}

const styles = StyleSheet.create({
    buttonContainer: {
        paddingHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    button: {
        width: 120,
        marginBottom: 20
    }
})
export default MyButton;