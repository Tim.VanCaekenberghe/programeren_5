import React from 'react';
import {StyleSheet, View, Button, Text, Platform} from 'react-native';
import Colors from "../constants/Colors";

const MyButton2 = props => {
    return(
        <View style={styles.buttonContainer}>
            <Button style={styles.button} color={(Platform.OS === "ios") ? "white" : Colors.PRIMARY} title='Back'>
            </Button>
        </View>
    );
};

const styles = StyleSheet.create({
    buttonContainer: {
        width: "50%",
        backgroundColor: (Platform.OS === "ios") ? Colors.PRIMARY : "rgba(0,0,0,0)",
        borderRadius: (Platform.OS === "ios") ? 5 : 0,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        padding: 10,
    }
});

export default MyButton2;
