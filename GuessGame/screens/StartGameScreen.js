import React, {useState} from 'react';
import {View, StyleSheet, Text, Alert, Image} from 'react-native';
import Colors from "../constants/Colors";
import Card from "../components/Card";
import MyTitle from "../components/MyTitle";
import MyButton from "../components/MyButton";
import MyInput from "../components/MyInput";
import MyText from "../components/MyText";
import GuessingScreen from "./GuessingScreen";
import DefaultStyle from "../constants/DefaultStyle";

const StartGameScreen = props => {
    const [enteredValue, setEnteredValue] = useState('');
    const [selectedNumber, setSelectedNumber] = useState();



    const numberInputHandler = inputText => {
        setEnteredValue(inputText.replace(/[^0-9]/g, ''));
    };

    const resetInputHandler = () => {
        setEnteredValue('');
    };

    const [confirmed, setConfirmed] = useState(false);

    const confirmInputHandler = () => {
        if (enteredValue !== "") {
            setSelectedNumber(parseInt(enteredValue));
            setConfirmed(true);
        } else {
            Alert.alert('Warning', 'Gelieve een getal in te voeren')
        }
    }
    let confirmedOutput;
    if (confirmed) {
        confirmedOutput = <Card style={styles.inputContainer}>
            <MyText style={styles.myText}>Chosen Number: {selectedNumber}</MyText>
        <MyButton text='Start Guessing'
                  onPress={()=> props.onStartGame(selectedNumber)}
            />
        </Card>

    }


        return (
            <View style={styles.container}>
                <MyTitle> The Game Screen</MyTitle>
                <Card style={styles.inputContainer}>
                    <MyText>Select a number</MyText>
                    <MyInput
                        style={styles.input}
                        keyboardType='number-pad'
                        maxLength={2}
                        onChangeText={numberInputHandler}
                        value={enteredValue}
                    />
                    <View style={styles.buttonContainer}>
                        <MyButton
                            text="Reset"
                            onPress={() => resetInputHandler()}
                        />
                        <MyButton
                            text="Confirm"
                            onPress={() => confirmInputHandler()}
                        />
                    </View>
                </Card>
                {confirmedOutput}
                <View style={styles.imgContainer}>
                    <Image source={require('../assets/guessing.png')}
                           style={styles.img}/>
                </View>
            </View>
        )
    }
    const styles = StyleSheet.create({
        container: {
            alignItems: 'center',
            padding: 10
        },
        imgContainer:{
            width: '80%',
            justifyContent: 'center',
            alignItems: 'center',
            height:160,
            borderRadius: 150,
            borderColor:Colors.accent,
            borderWidth:2,
            overflow:'hidden',
            marginTop: 10
        },
        inputContainer: {
            backgroundColor: Colors.primary,
            width: 300,
            maxWidth: '80%',
            borderRadius: 20,
            shadowColor: 'grey',
            shadowOffset: {width: 2, height: 2},
            shadowRadius: 6,
            shadowOpacity: 0.9,
            elevation: 8,
            alignItems: 'center',
            paddingVertical: 20,
            marginBottom: 20
        },
        buttonContainer: {
            paddingHorizontal: 15,
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '100%',
        },
        myText:{
            padding: 10
        }
    })
export default StartGameScreen;