import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import Colors from "../constants/Colors";
import Card from "../components/Card";
import MyTitle from "../components/MyTitle";
import MyText from "../components/MyText";
import MyInput from "../components/MyInput";

const Home = props =>{
    const [enteredValue, setEnteredValue] = useState('');

    const numberInputHandler = inputText => {
        setEnteredValue(inputText.replace(/[^a-zA-Z ]/g, ''));
    };
    return(
        <View style={styles.container}>
            <Card style={styles.inputContainer}>
                <MyTitle>The Guessing Game App</MyTitle>
            <MyText>Welcome to the guessing game</MyText>
                <MyInput
                    style={styles.input}
                    onChangeText={numberInputHandler}
                    value={enteredValue}
                />

            </Card>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 10
    },
    input:{
        width: '80%'
    },
    inputContainer: {
        backgroundColor: Colors.primary,
        width: 300,
        maxWidth: '80%',
        borderRadius: 20,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        alignItems: 'center',
        paddingVertical: 20
    }
})
export default Home;