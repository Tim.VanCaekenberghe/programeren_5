import React, {useState, useRef, useEffect} from 'react';
import {View, StyleSheet, Text, Image, TouchableOpacity, Alert, ScrollView} from 'react-native';
import Colors from "../constants/Colors";
import Card from "../components/Card";
import MyTitle from "../components/MyTitle";
import MyButton from "../components/MyButton";
import MyInput from "../components/MyInput";
import MyText from "../components/MyText";
import {AntDesign, MaterialIcons} from '@expo/vector-icons';

const GuessingScreen = props => {
    const generateRandomBetween = (min, max, exclude) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        const rndNum = Math.floor(Math.random() * (max - min)) + min;
        //recursion in case the games guesses the right number on the first go
         if (rndNum === exclude) {
        return generateRandomBetween(min, max, exclude);
         }else {
             return rndNum;
         }
    }
    const initialGuess = generateRandomBetween(1, 100, props.userChoice);
    const [currentGuess, setCurrentGuess] = useState(initialGuess);
    const[appGuesses, setAppGuesses] = useState([initialGuess]);
    useState(initialGuess);



    const currentLow = useRef(1);
    const currentHigh = useRef(100);




    const nextGuessHandler = direction => {
         if(
             (direction === 'lower' && currentGuess < props.userChoice)||
             (direction === 'higher' && currentGuess > props.userChoice)){
             Alert.alert('You liar','You know that this is wrong',[{text:'Oeps', style:'cancel'}]);
             return;
         }
        if (direction === 'lower' ){
            currentHigh.current = currentGuess;
        }else{
            currentLow.current = currentGuess;
        }
        const nextNumber =
            generateRandomBetween(currentLow.current,currentHigh.current,currentGuess);
        setCurrentGuess(nextNumber);
        setAppGuesses(pastGuesses =>[nextNumber,...pastGuesses]);
    }




    useEffect(() =>{
        if(currentGuess === props.userChoice){
        props.onGameOver(appGuesses.length);
    }
        }, [currentGuess, props.userChoice, props.onGameOver]);


    return (

            <View style={styles.container}>
            <MyTitle> The Guessing Screen</MyTitle>
            <Card style={styles.inputContainer}>
                <MyText style={styles.myText}>Opponents Guess: {currentGuess}</MyText>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity
                        onPress={nextGuessHandler.bind(this,'higher')}
                    >
                        <AntDesign name="arrowup" size={60} color="black" />
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={nextGuessHandler.bind(this,'lower')}
                    >
                        <AntDesign name="arrowdown" size={60} color="black" />
                    </TouchableOpacity>
                </View>
            </Card>
            <ScrollView>
                <Card style={styles.inputContainer}>
                {appGuesses.map((guess, index) =>
                        <MyText style={styles.listItem}>Guess #{index + 1}:  {guess}</MyText>)}
                </Card>
            </ScrollView>
            </View>
    )
}
const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        padding: 10
    },
    inputContainer: {
        backgroundColor: Colors.primary,
        width: 300,
        maxWidth: '100%',
        borderRadius: 20,
        shadowColor: 'grey',
        shadowOffset: {width: 2, height: 2},
        shadowRadius: 6,
        shadowOpacity: 0.9,
        elevation: 8,
        alignItems: 'center',
        paddingVertical: 20,
        marginBottom: 20
    },
    buttonContainer: {
        paddingHorizontal: 15,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    myText:{
        padding: 10,
        marginBottom: 10
    },
    listItem:{
        borderWidth: 1,
        width: 200,
        padding: 2,
        marginVertical: 5,
        textAlign: 'center'


    }
})
export default GuessingScreen;
