import React, {Component} from 'react';
import {StyleSheet, Button, Text, View, TextInput, Alert, ImageBackground} from 'react-native';

var myBackground = require('./assets/image11.png');
export default function App() {
    const [value, onChangeText] = React.useState('Placeholder');
    return (
        <View style={styles.container}>
            <ImageBackground source= {myBackground} style={{width: '100%', height: '100%'}}>
                <Text style={styles.paragraph}>Hello World!</Text>
                <TextInput
                    style={styles.inputStyle}
                />
                <Button
                    style={{width: 100}}
                    title="Press me"
                    onPress={() => Alert.alert('Simple Button pressed')}
                />
            </ImageBackground>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#8d6e63',
        alignItems: 'center',
        justifyContent: 'center'
    },
    paragraph: {
        color: '#ffffff'

    },
    inputStyle: {
        height: 40,
        width: 300,
        borderWidth: 1,
        margin: 4
    }
});
