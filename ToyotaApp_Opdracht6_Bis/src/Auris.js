import React from 'react';
import {StyleSheet, Image, Button, ScrollView, View} from 'react-native';
import {Content, Accordion, Thumbnail, Header, Card, Text, Body, ListItem, List} from 'native-base';
import data from '../toyota';


class Auris extends React.Component {
    render() {
        let car = data.cars[2];
        const dataArray = [
            {title: "Prijs", content: car.prijs},
            {title: "Verbuik", content: car.verbruik},
            {title: "Motor", content: car.motor},
            {title: "Capaciteit", content: car.capaciteit}
        ];
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Text style={styles.header}>{car.name}</Text>
                    <View style={styles.viewStyle}>
                        <Thumbnail large
                                   source={{uri: car.detailimg}}
                                   style={styles.img}/>
                    </View>
                    <ListItem itemDivider
                              style={{marginTop: 10}}
                    >
                        <Text style={{fontWeight: 'bold'}}>Specificaties: </Text>
                    </ListItem>
                    <List>
                        <ListItem>
                            <Text>{car.spec}</Text>
                        </ListItem>
                        <ListItem>
                            <Text>{car.spec1}</Text>
                        </ListItem>
                        <ListItem>
                            <Text>{car.spec2}</Text>
                        </ListItem>
                        <ListItem>
                            <Text>{car.spec3}</Text>
                        </ListItem>

                    </List>

                    <Content>
                        <Accordion dataArray={dataArray} icon="add" expandedIcon="remove"/>
                    </Content>
                    <View style={styles.buttonContainer}>
                        <Button style={styles.button}
                                primary
                                title={"Back"}
                                onPress={() => this.props.switchScreen("Cars")}
                        ><Text style={styles.buttonText}>Ga terug</Text></Button>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 45,
        color: 'red',
        textAlign: 'center',
        justifyContent: 'center',
        margin: 20
    },
    buttonText: {
        color: 'white',
        fontSize: 15,
    },
    button: {
        flex: 1,
        width: '100%',
        borderRadius: 10,
        backgroundColor: 'red',
        justifyContent: 'center'
    },
    buttonContainer: {
        justifyContent: 'space-between',
        alignItems: 'center',
        textAlign: 'center'
    },
    viewStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    img: {
        height: 160,
        width: '98%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    info: {
        flex: 1,
        backgroundColor: 'white',
        opacity: 0.6
    }
});

export default Auris;