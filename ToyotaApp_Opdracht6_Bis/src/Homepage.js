import React from 'react';
import {Text, View, ImageBackground, Image, Platform} from 'react-native';
import {Button, Content} from 'native-base';

let background = require('../assets/background.png');


export default class Homepage extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={background} style={{width: '100%', height: '100%', opacity: 0.9}}>
                    <View style={styles.img}>
                        <Image source={{uri:'http://4.bp.blogspot.com/-dUXWiRkjQKo/UCmrwFIR_YI/AAAAAAAABKc/xCaQPhOqUas/s1600/toyota_logo_1.jpg'}}
                               style={{width: 200, height: 150}}
                        />
                    </View>
                    <Content contentContainerStyle={styles.viewcontainer}>
                        <Text style={styles.titleStyle}>Welkom!</Text>
                        <Text style={styles.text}>Bekijk hier onze voertuigen</Text>
                        <View style={styles.alternativeLayoutButtonContainer}>
                            <Button style={styles.button}
                                    primary
                                    block={true}
                                    onPress={() => this.props.switchScreen("Cars")}
                            >
                                <Text style={styles.buttonText}>Voertuigen</Text>
                            </Button>
                        </View>
                    </Content>
                </ImageBackground>
            </View>

        )
    }
}

const styles = {
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    titleStyle: {
        fontSize: 35,
        color: 'black',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
        backgroundColor: 'red',
        opacity: 0.8,
        borderStyle: 'solid'
    },
    img:{
        marginTop: 60,
      justifyContent: 'center',
        alignItems: 'center'
    },
    buttonSpace:{
        marginLeft: 25
    },
    alternativeLayoutButtonContainer: {
        margin: 50,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    button: {
        flex: 1,
        width: '95%',
        borderRadius: 10,
        backgroundColor: 'red'
    },
    buttonText: {
      color: 'white',
        fontSize: 15
    },
    text: {
        justifyContent: 'center',
        alignItems: 'center',
        fontSize:18,
        backgroundColor: 'red',
        opacity: 0.8,
        color: 'white'
    },
    viewcontainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 20
    }
}