import React from 'react';
import { View } from 'react-native';
import Homepage from "./src/Homepage";
import Cars from './src/Cars'
import Gt86 from "./src/Gt86";
import LandCruiser from "./src/LandCruiser";
import Auris from "./src/Auris";

export default class App extends React.Component {

    state = {
        currentScreen: "Homepage",
    }
    switchScreen = (currentScreen) => {
        this.setState({currentScreen});
    }


    renderScreen = () => {
        switch (this.state.currentScreen) {
            case "Homepage":
                return (
                    <Homepage switchScreen={this.switchScreen}/>
                );
                break;
            case "Cars":
                return (
                    <Cars switchScreen={this.switchScreen}/>
                );
                break;
            case "GT-86":
                return (
                    <Gt86 switchScreen={this.switchScreen}/>
                )
            case "Land Cruiser":
                return (
                    <LandCruiser switchScreen={this.switchScreen}/>
                )
            case "Auris":
                return (
                    <Auris switchScreen={this.switchScreen}/>
                )
            case "Prius":
                return (
                    <Pris switchScreen={this.switchScreen}/>
                )
            case "Yaris":
                return (
                    <Yaris switchScreen={this.switchScreen}/>
                )
            case "Aygo":
                return (
                    <Aygo switchScreen={this.switchScreen}/>
                )
        }
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderScreen()}
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1
    }
}

